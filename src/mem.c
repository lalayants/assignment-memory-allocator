#include <stdarg.h>

#define _DEFAULT_SOURCE

#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header *b, const char *fmt, ...);

void debug(const char *fmt, ...);

extern inline block_size size_from_capacity(block_capacity cap);

extern inline block_capacity capacity_from_size(block_size sz);

static bool block_is_big_enough(size_t query, struct block_header *block) { return block->capacity.bytes >= query; }

static size_t pages_count(size_t mem) { return mem / getpagesize() + ((mem % getpagesize()) > 0); }

static size_t round_pages(size_t mem) { return getpagesize() * pages_count(mem); }

static void block_init(void *restrict addr, block_size block_sz, void *restrict next) {
    *((struct block_header *) addr) = (struct block_header) {
            .next = next,
            .capacity = capacity_from_size(block_sz),
            .is_free = true
    };
}

static size_t region_actual_size(size_t query) { return size_max(round_pages(query), REGION_MIN_SIZE); }

extern inline bool region_is_invalid(const struct region *r);


static void *map_pages(void const *addr, size_t length, int additional_flags) {
    return mmap((void *) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags, 0, 0);
}

/*  allocate region and initialize with block */
static struct region alloc_region(void const *addr, size_t query) {
  if (!addr) return REGION_INVALID;
   
  struct region new_region = {.addr = map_pages(addr, region_actual_size(query), MAP_FIXED_NOREPLACE), .size=region_actual_size(query)};
  if (new_region.addr != MAP_FAILED){
      new_region.extends = true;
  } else {
    new_region.addr = map_pages(addr, region_actual_size(query), MAP_FIXED);
    if (new_region.addr == MAP_FAILED) return REGION_INVALID;
    new_region.extends = false;
  }
  block_init(new_region.addr, (block_size) {.bytes = region_actual_size(query)}, NULL);
  return new_region;
}

static void *block_after(struct block_header const *block);

void *heap_init(size_t initial) {
    const struct region region = alloc_region(HEAP_START, initial);
    if (region_is_invalid(&region)) return NULL;

    return region.addr;
}

#define BLOCK_MIN_CAPACITY 24

/*  --- Split block if the one you find too big--- */

static bool block_splittable(struct block_header *restrict block, size_t query) {
    return block->is_free &&
           query + offsetof(struct block_header, contents) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

static bool split_if_too_big(struct block_header *block, size_t query) {
  query = BLOCK_MIN_CAPACITY > query ? BLOCK_MIN_CAPACITY : query;
  if (block_splittable(block, query)){
    block_init(block->contents + query, 
      (block_size) {size_from_capacity(block->capacity).bytes - size_from_capacity((block_capacity){.bytes=query}).bytes},
      block->next);
    block->next = (struct block_header *) (block->contents + query);
    block->capacity = (block_capacity) {.bytes = query};
    return true;
  }
  return false;
}


/*  --- Merge free blocks --- */

static void *block_after(struct block_header const *block) {
    return (void *) (block->contents + block->capacity.bytes);
}

static bool blocks_continuous(
        struct block_header const *fst,
        struct block_header const *snd) {
    return (void *) snd == block_after(fst);
}

static bool mergeable(struct block_header const *restrict fst, struct block_header const *restrict snd) {
    return fst->is_free && snd->is_free && blocks_continuous(fst, snd);
}

static bool try_merge_with_next(struct block_header *block) {
   if (block->next && mergeable(block, block->next)){
      block->capacity = (block_capacity) {block->capacity.bytes + size_from_capacity(block->next->capacity).bytes};
      block->next = block->next->next;
    return true;
  }
  return false;
}


/*  --- if heap is big enough --- */

struct block_search_result {
    enum {
        BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED
    } type;
    struct block_header *block;
};

static struct block_search_result lock_search_result_found(struct block_header * bh){
  return (struct block_search_result) {.type = BSR_FOUND_GOOD_BLOCK, .block=bh};
}

static struct block_search_result lock_search_result_reached_end_not_found(struct block_header * end){
  return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block=end};
}

static struct block_search_result lock_search_result_corrupted(){
  return (struct block_search_result) {.type = BSR_REACHED_END_NOT_FOUND, .block=NULL};
}

static struct block_search_result find_good_or_last(struct block_header *restrict block, size_t sz) {
  if (!sz) return lock_search_result_corrupted();
  
  struct block_header * end = 0;
  for (struct block_header * bh = block; bh; bh = bh->next){
    while(try_merge_with_next(bh));
    if (bh->is_free && block_is_big_enough(sz, bh)){
      split_if_too_big(bh, sz);
      return lock_search_result_found(bh);
    }
    if (!bh->next) end = bh;
  }

  return lock_search_result_reached_end_not_found(end);
}

/*  Try to alloc in heap starting with block `block` without expending heap
 Can be reused after heap expension*/
static struct block_search_result try_memalloc_existing(size_t query, struct block_header *block) {
  struct block_search_result res_block = find_good_or_last(block, query);
  if (res_block.type == BSR_FOUND_GOOD_BLOCK) res_block.block->is_free = false;
  return res_block;
}


static struct block_header *grow_heap(struct block_header *restrict last, size_t query) {
    /*  ??? */
  if (!last) return NULL;
  
  struct region new_reg = alloc_region(last->contents + last->capacity.bytes, size_from_capacity((block_capacity) {query}).bytes);

  if (region_is_invalid(&new_reg)) return NULL;

  last->next = (struct block_header *) new_reg.addr;
  return last->next;
}

/*  returns header of new block */
static struct block_header *memalloc(size_t query, struct block_header *heap_start) {
     /*  ??? */
  if (!heap_start) return NULL;
  // printf("memalloc not fisrt null\n");
  struct block_search_result search_res = try_memalloc_existing(query, heap_start);
  if (search_res.type != BSR_REACHED_END_NOT_FOUND) return search_res.block;
  if (grow_heap(search_res.block, query)){
    search_res = try_memalloc_existing(query, heap_start);
    return search_res.block;
  }
  // printf("memalloc second null\n");
  return NULL;
}


void *_malloc(size_t query) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

struct block_header *block_get_header(void *contents) {
    return (struct block_header *) (((uint8_t *) contents) - offsetof(struct block_header, contents));
}

void _free(void *mem) {
    if (!mem){
      //printf("Ya tyt\n");
      return;
    }
    struct block_header *header = block_get_header(mem);
    header->is_free = true;
    while(try_merge_with_next(header));
}