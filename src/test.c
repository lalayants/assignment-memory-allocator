#include "test.h"
// #include "mem.h"

static int8_t test_heap_creation(void){
    printf("TEST#0:\t===Create HEAP===\n");
    void* heap = heap_init(15000);
    if (!heap){
        fprintf(stderr, "TEST#0: ERROR\nTests stopped\n");
        return 1;
    }
    fprintf(stderr, "TEST#0: PASSED\n\n");
    return 0;
}

static int test_memory_allocation_and_free(void){
    printf("TEST#1:\t===Create 123 and clear 123===\n");
    void* p123 = _malloc(123);
    struct block_header* b123 = block_get_header(p123);
    if (!(b123 && b123->capacity.bytes == 123 && !b123->is_free)){
        fprintf(stderr, "TEST#1: ERROR! Memory allocation failed\n Tests stopped\n");
        return 1;
    }
    fprintf(stdout, "TEST#1: Memory allocated succesfully\n");

    _free(p123);
    if (b123->is_free){
        fprintf(stdout, "TEST#1: Memory free succesfully\n");
        fprintf(stdout, "TEST#1: PASSED\n\n");
        return 0;
    }
    fprintf(stderr, "TEST#1: ERROR! Memory allocation failed\nTests stopped\n");
    return 1;
}

static int test_memory_allocation_of_small_region(void){
    printf("TEST#2:\t===Create 1 (to small, must be 24)===\n");
    void* p1 = _malloc(1);
    struct block_header* b1 = block_get_header(p1);
    if (!(b1 && b1->capacity.bytes == 24 && !b1->is_free)){
        fprintf(stderr, "TEST#2: ERROR! Memory allocation failed\n Tests stopped\n");
        return 1;
    }
    fprintf(stdout, "TEST#2: Memory allocated succesfully\n");
    _free(p1);
    fprintf(stdout, "TEST#2: PASSED\n\n");
    return 0;
}

static int test_memory_allocation_of_huge_region(void){
    printf("TEST#3:\t===Create 20000 (bigger than heap)===\n");
    void* p = _malloc(20000);
    struct block_header* b = block_get_header(p);
    if (!(b && b->capacity.bytes == 20000 && !b->is_free)){
        fprintf(stderr, "TEST#3: ERROR! Memory allocation failed\n Tests stopped\n");
        return 1;
    }
    fprintf(stdout, "TEST#3: Memory allocated succesfully\n");
    fprintf(stdout, "TEST#3: PASSED\n\n");
    _free(p);
    return 0;
}

static int test_memory_merge_after_free(void){
    printf("TEST#4:\t===Create 100, 200, 300; free 300, 200 (should merge together)===\n");
    void* p1 = _malloc(100);
    void* p2 = _malloc(200);
    void* p3 = _malloc(300);
    struct block_header* b = block_get_header(p2);
    _free(p3);
    _free(p2);
    if (!(b && b->capacity.bytes > 500 && b->is_free)){
        fprintf(stderr, "TEST#4: ERROR! Memory free and merge failed\n Tests stopped\n");
        return 1;
    }
    fprintf(stdout, "TEST#4: Memory free and merged succesfully\n");
    fprintf(stdout, "TEST#4: PASSED\n\n");
    _free(p1);
    return 0;
}



int8_t alloc_tests_execute(){
    if (test_heap_creation()) return 1;
    if (test_memory_allocation_and_free()) return 1;
    if (test_memory_allocation_of_small_region()) return 1;
    if (test_memory_allocation_of_huge_region()) return 1;
    if (test_memory_merge_after_free()) return 1;
    fprintf(stdout, "ALL TESTS PASSED! GOODBYE!\n");
    return 0;
}